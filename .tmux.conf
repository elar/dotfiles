#
# Colors
#
active_window_bg=colour34
inactive_window_bg=colour102
bar_bg=colour237
bar_fg=colour255

# reload
bind r source-file ~/.tmux.conf \; display "Reloaded!"
#
# General settings
#
set -g default-terminal screen.xterm-256color
set -g status-right ''
set -g status-left ''
set -g history-limit 100000
set -g escape-time 1
set -g base-index 1
set -g mode-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel
set-environment -g CHERE_INVOKING 1

unbind C-b
set -g prefix C-a
bind-key 'C-a' send-prefix

bind -n M-left  prev
bind -n M-right next
bind -n M-C-left  swap-window -t -1
bind -n M-C-right swap-window -t +1
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -n F1 select-pane -L
bind -n F2 select-pane -R
bind-key -n S-F2 send F2

# Copy to cygwin clipboard
bind -n C-y run "tmux save-buffer - > /dev/clipboard"

set -g mouse on


bind -n M-F11 set -qg window-status-current-style bg=$inactive_window_bg
bind -n M-F12 set -qg window-status-current-style bg=$active_window_bg
bind -n M-up \
             send-keys M-F12 \; \
             unbind -n M-left \; \
             unbind -n M-right \; \
             unbind -n M-C-left \; \
             unbind -n M-C-right \; \
             set -qg window-status-current-style bg=$inactive_window_bg \; \
             unbind -n | \; \
             unbind -n - \; \
             unbind -n h \; \
             unbind -n j \; \
             unbind -n k \; \
             unbind -n l \; \
             unbind -n F1 \; \
             unbind -n F2 \; \
             unbind -n S-F2 \; \
             set -qg prefix C-j
bind -n M-down \
     send-keys M-F11 \; \
     bind -n M-left  prev \; \
     bind -n M-right next \; \
     bind -n M-C-left swap-window -t -1 \; \
     bind -n M-C-right swap-window -t +1 \; \
     set -qg window-status-current-style bg=$active_window_bg \; \
     bind | split-window -h -c "#{pane_current_path}" \; \
     bind - split-window -v -c "#{pane_current_path}" \; \
     bind h select-pane -L \; \
     bind j select-pane -D \; \
     bind k select-pane -U \; \
     bind l select-pane -R \; \
     bind -n F1 select-pane -L \; \
     bind -n F2 select-pane -R \; \
     bind-key -n S-F2 send F2 \; \
     set -qg prefix C-a

#
# Appearance
#
set -g status-style bg=$bar_bg
setw -g window-status-style fg=$bar_fg
setw -g window-status-current-format ' #I #W '
setw -g window-status-format ' #I #W '
setw -g window-status-current-style bg=$active_window_bg
