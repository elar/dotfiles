PS1='\t \h `pwd | sed -e "s/\/\(.\)[^\/]\+/\/\1/g"` \$ '

export ENV=~/.env
export PATH=~/bin:$PATH
export EDITOR=vim
export LANG=en_US.UTF-8
export HISTSIZE=65536

[ -e $ENV ] && . $ENV

